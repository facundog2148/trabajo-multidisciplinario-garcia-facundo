﻿using System;

namespace Ejercicio_1
{
    public class Cuenta
    {
        private string titular;
        private double cantidad;

        public Cuenta(string titular)
        {
            this.titular = titular;
        }
        public Cuenta(string titular, double cantidad)
        {
            this.titular = titular;
            if(cantidad < 0)
            {
                this.cantidad = 0;
            } else
            {
                this.cantidad = cantidad;
            }
        }

        public void setTitular (string titular)
        {
            this.titular = titular;
        }

        public string getTitular()
        {
            return titular;
        }
        

        public void setCantidad(double cantidad)
        {
            this.cantidad = cantidad;
        }

        public double getCantidad()
        {
            return cantidad;
        }


        public void ingresar(double cantidad)
        {
            if(cantidad > 0)
            {
                this.cantidad += cantidad;
            }
        }

        public void retirar(double cantidad)
        {
            if(this.cantidad - cantidad < 0)
            {
                this.cantidad = 0;
            }else
            {
                this.cantidad -= cantidad;
            }
        }

    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Cuenta cuenta1 = new Cuenta("Facundo");
            Cuenta cuenta2 = new Cuenta("Pedro", 700);

            cuenta1.ingresar(500);
            cuenta2.ingresar(200);

            cuenta1.retirar(400);
            cuenta2.retirar(100);

            Console.WriteLine(cuenta1);
            Console.WriteLine(cuenta2);
        }
    }
}
