﻿using System;

namespace Ejercicio6
{
    public class Libro
    {
        private int ISBN;
        private string titulo;
        private string autor;
        private int numdePaginas;

        public Libro(int ISBN, string titulo, string autor, int numdePaginas)
        {
            this.ISBN = ISBN;
            this.titulo = titulo;
            this.autor = autor;
            this.numdePaginas = numdePaginas;
        }

        public int isbn
        {
            get
            {
                return ISBN;
            }
            set
            {
                this.isbn = ISBN;
            }
        }

        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                this.Titulo = titulo;
            }
        }

        public string Autor
        {
            get
            {
                return autor;
            }
            set
            {
                this.Autor = autor;
            }
        }

        public int getnumdePaginas()
        {
            return numdePaginas;
        }

        public int setnumdePaginas(int numdePaginas)
        {
            this.numdePaginas = numdePaginas;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Libro libro1 = new Libro(33333333, "Cronica de una muerte anunciada", "Gabriel Garcia Marques", "90");
            Libro libro2 = new Libro(33333334, "Martin Fierro", "Jose Hernandez", "300");

            Console.WriteLine(libro1.Informacion());
            if (libro1.getnumdePaginas() > libro2.getnumdePaginas())
            {
                Console.WriteLine(libro1.Titulo + "tiene más paginas");
            }
            else
            {
                Console.WriteLine(libro2.Titulo + "tiene más páginas");

                Console.ReadKey();
            }
        }
    }
}
