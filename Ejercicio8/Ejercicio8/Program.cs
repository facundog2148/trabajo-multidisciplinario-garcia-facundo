﻿using System;

namespace Ejercicio8
{
    public abstract class Persona
    {
        const string[] nombreAlumnos = { "Lucas", "Ignacio", "Pablo", "Franco" };
        const string[] nombreAlumnas = { "Maria", "Ana", "Juana", "Sofia" };

        private string nombre;
        private char sexo;
        private int edad;
        private Boolean asistencia;

        public Persona()
        {

        }

        public string getnombre()
        {
            return nombre;
        }

        public void setnombre(string nombre)
        {
            this.nombre = nombre;
        }

        public char getsexo()
        {
            return sexo;
        }

        public void setsexo(char sexo)
        {
            this.sexo = sexo;
        }

        public int getedad()
        {
            return edad;
        }

        public void setedad(int edad)
        {
            this.edad = edad;
        }

        public Boolean getasistencia()
        {
            return asistencia;
        }

        public void setasistencia(Boolean asistencia)
        {
            this.asistencia = asistencia;
        }

        public abstract void disponibilidad();
    }

    public class Profesor : Persona
    {
        private string materia;
        public Profesor()
        {
            
        }

        public string getmateria()
        {
            return materia;
        }

        public void setmateria(string materia)
        {
            this.materia = materia;
        }

        public override void disponibilidad()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            float num = rnd.Next(0, 100);
            this.asistencia = (num < 20) ? false : true;
        }

    }

    public class Alumno : Persona
    {
        private int notas;
        public Alumno()
        {
            
        }

        public int getnotas()
        {
            return notas;
        }

        public void getnotas(int notas)
        {
            this.notas = notas;
        }
        public override void disponibilidad()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            float num = rnd.Next(0, 100);
            this.asistencia = (num < 50) ? false : true;
        }

    }

    public class Aula
    {
        private int id;
        private Profesor profesor;
        private List<Estudiante> estudiantes;
        private string materia;

        public Aula(int id, List<string> nombres, List<char> sexo, List<int> edades, List<int> notas)
        {
            for (int i = 0; i < nombres.Count; i++)
            {
                estudiantes.Add(new Estudiante(nombres[i], sexo[i], edades[i], notas[i]));
            }
        }

        public bool AsistenciaDeAlumnos()
        {
            int presentes = 0;
            foreach (Estudiante e in estudiantes)
            {
                e.disponibilidad();
                if (e.getAsistencia)
                {
                    presentes++;
                }
            }
            return (presentes >= Math.Round((decimal)estudiantes.Count / 2));
        }

        public bool darClase()
        {
            profesor.disponibilidad();
            if (!profesor.getAsistencia)
            {
                return false;
            }
            if (profesor.getMateria != this.materia)
            {
                Console.WriteLine("No se puede dar clase, el profesor no pertenece a la materia.");
                return false;
            }
            if (!this.asistenciaAlumnos())
            {
                Console.WriteLine("No se puede dar clase, la sistencia es insuficiente.");
                return false;
            }
            Console.WriteLine("Se puede dar clases.");
            return true;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
