﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    public class Juego
    {
        private Jugador[] jugadores;
        private Revolver revolver;

        public Juego(int numjugadores)
        {

            jugadores = new Jugador[comprobarJugadores(numjugadores)];
            crearJugadores();
            revolver = new Revolver();

        }
        private int comprobarJugadores(int numjugadores)
        {

            if (!(numjugadores >= 1 && numjugadores <= 6))
            {
                numjugadores = 6;
            }

            return numjugadores;
        }
        private void crearJugadores()
        {
            for (int i = 0; i < jugadores.Length; i++)
            {
                jugadores[i] = new Jugador(i + 1);
            }
        }
        public bool finJuego()
        {

            for (int i = 0; i < jugadores.Length; i++)
            {
                if (!jugadores[i].Vivo())
                {
                    return true;
                }
            }

            return false;

        }
        public void ronda()
        {
           for(int i = 0; i < jugadores.Length && jugadores[i].Vivo(); i++)
            {
                jugadores[i].disparar(this.revolver);
                if (!jugadores[i].Vivo()){
                    Console.WriteLine("Game over");
                    break;
                }
                else
                {
                    Console.WriteLine("El juego continúa");
                }
            }
        }

    }
}
