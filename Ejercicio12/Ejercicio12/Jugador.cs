﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    public class Jugador
    {
        private int id = 1;
        private string nombre;
        private bool vivo;

        public Jugador(int id)
        {
            this.id = id;
            this.nombre = "Jugador " + id;
            this.vivo = true;
        }

        public void disparar(Revolver r)
        {

            Console.WriteLine("El" + nombre + " se apunta con la pistola\n");

            if (r.disparar())
            {
                vivo = false;
                Console.WriteLine("El " + nombre + " ha muerto\n");
            }
            else
            {
                Console.WriteLine("El " + nombre + " sigue vivo\n");
            }
        }

        public bool Vivo()
        {
            return vivo;
        }

    }
}
