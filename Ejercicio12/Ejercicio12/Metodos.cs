﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    public class Metodos
    {
        public static int generaNumeroAleatorio(int minimo, int maximo)
        {
            Random r = new Random();
            int num = r.Next((maximo - minimo + 1) + (minimo));
            return num;
        }
    }
}
