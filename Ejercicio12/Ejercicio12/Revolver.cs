﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12
{
    public class Revolver
    {
        private int posicion_actual;
        private int posicion_bala;

        public Revolver()
        {
            this.posicion_actual = Metodos.generaNumeroAleatorio(1, 6);
            this.posicion_bala = Metodos.generaNumeroAleatorio(1, 6);
        }

        public bool disparar()
        {
            bool exit = false;

            if(posicion_actual == posicion_bala)
            {
                exit = true;
            }
            siguientebala();

            return exit;
        }

        public void siguientebala()
        {
            if(posicion_actual == 6)
            {
                posicion_actual = 1;
            }else
            {
                posicion_actual++;
            }

        }

    }
}
