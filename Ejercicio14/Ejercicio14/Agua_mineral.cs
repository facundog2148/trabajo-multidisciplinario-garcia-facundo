﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio14
{
    public class Agua_mineral : Bebida
    {
        private string origen;

        public String getOrigen()
        {
            return origen;
        }

        public void setOrigen(String origen)
        {
            this.origen = origen;
        }

        public Agua_mineral(int id, int cantidadLitros, double precio, String marca, String origen) : base(id, cantidadLitros, precio, marca)
        {
            this.origen = origen;
        }

    }
}
