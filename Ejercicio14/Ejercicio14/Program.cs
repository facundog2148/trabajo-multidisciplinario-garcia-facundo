﻿using System;

namespace Ejercicio14
{
    class Program
    {
        static void Main(string[] args)
        {
            Almacen alm = new Almacen(3, 4);
            Bebida vino = new Bebida(5, 1, 200, "Malbec");
            Bebida cerveza = new Bebida(30, 2, .100, "Quilmes");
            Agua_mineral agua1 = new Agua_mineral(50, 1, 2, "Villavicencio", "Argentina");
            Agua_mineral agua2 = new Agua_mineral(25, 5, 2.33, "Glaciar", "Argentina");
            Bebida_azucarada jugo1 = new Bebida_azucarada(55, false, 3, 2, 60, "Pepsi");
            Bebida_azucarada jugo2 = new Bebida_azucarada(60, true, 4, 1, 45, "Citric");


            alm.agregarProducto(vino);
            alm.agregarProducto(jugo2);
            alm.mostrarInformacionProductos();
            Console.WriteLine("");

            alm.eliminarProducto(50);
            alm.mostrarInformacionProductos();
            Console.WriteLine("");

            alm.agregarProducto(jugo1);
            alm.agregarProducto(agua1);
            Console.WriteLine(alm.calcularPrecioTotalProductos());
            Console.WriteLine("");

            Console.WriteLine(alm.calcularPrecioTotalMarca("Pepsi"));
            Console.WriteLine("");


            alm.eliminarProducto(45);
            alm.eliminarProducto(55);
            alm.eliminarProducto(65);
            alm.mostrarInformacionProductos();

            alm.agregarProducto(vino);
            alm.agregarProducto(cerveza);
            alm.agregarProducto(agua1);
            alm.agregarProducto(agua2);
            alm.agregarProducto(jugo1);
            alm.agregarProducto(jugo2);
            alm.mostrarInformacionProductos();
            Console.WriteLine("");

            Console.WriteLine(alm.calcularPrecioTotalColumna(1));
            Console.WriteLine(alm.calcularPrecioTotalMarca("Glaciar"));
            Console.WriteLine(alm.calcularPrecioTotalProductos());

        }
    }
}
