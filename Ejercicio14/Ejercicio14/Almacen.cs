﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio14
{
    public class Almacen : Bebida
    {
        private Bebida[][] estanteria;

        public Bebida[][] getEstanteria()
        {
            return estanteria;
        }

        public void setEstanteria(Bebida[][] estanteria)
        {
            this.estanteria = estanteria;
        }

        public Almacen(int filas, int columnas)
        {
            estanteria = new Bebida[filas][columnas];
        }

        public Almacen()
        {
            estanteria = new Bebida[5][5];
        }

        public double calcularPrecioTotalProductos()
        {
            double precioTotalProductos = 0.0;
            for (int i = 0; i < estanteria.Length; i++)
            {
                for (int j = 0; j < estanteria[0].Length; j++)
                {
                    if (estanteria[i][j] != null)
                    {
                        precioTotalProductos += estanteria[i][j].getPrecio();
                    }

                }
            }
            return precioTotalProductos;
        }

        public double calcularPrecioTotalMarca(String marca)
        {
            double precioTotalMarca = 0.0;
            for (int i = 0; i < estanteria.Length; i++)
            {
                for (int j = 0; j < estanteria[0].Length; j++)
                {
                    if (estanteria[i][j] != null)
                    {
                        if (estanteria[i][j].getMarca() == marca)
                        {
                            precioTotalMarca += estanteria[i][j].getPrecio();

                        }

                    }
                }
            }
            return precioTotalMarca;
        }

        public double calcularPrecioTotalColumna(int columna)
        {
            double precioTotalColumna = 0.0;
            for (int i = 0; i < estanteria.Length; i++)
            {
                for (int j = 0; j < estanteria[0].Length; j++)
                {
                    if (j == columna)
                    {
                        precioTotalColumna += estanteria[i][j].getPrecio();
                    }
                }
            }
            return precioTotalColumna;
        }

        public void agregarProducto(Bebida b)
        {
            Boolean encontrado = false;
            for (int i = 0; i < estanteria.Length && !encontrado; i++)
            {
                for (int j = 0; j < estanteria[0].Length && !encontrado; j++)
                {
                    if (estanteria[i][j] != null)
                    {
                        if (estanteria[i][j].getId() == b.getId())
                        {
                            encontrado = true;
                        }
                    }
                    else
                    {
                        if (estanteria[i][j] == null)
                        {
                            estanteria[i][j] = b;
                            encontrado = true;

                        }

                    }
                }

            }
        }

        public void eliminarProducto(int id)
        {
            for (int i = 0; i < estanteria.Length; i++)
            {
                for (int j = 0; j < estanteria[0].Length; j++)
                {
                    if (estanteria[i][j] != null && estanteria[i][j].getId() == id)
                    {
                        estanteria[i][j] = null;
                    }
                }
            }
        }

        public void mostrarInformacionProductos()
        {
            for (int i = 0; i < estanteria.Length; i++)
            {
                for (int j = 0; j < estanteria[0].Length; j++)
                {
                    if (estanteria[i][j] != null)
                    {
                        Console.WriteLine(
                                "En la fila " + i + " y columna " + j + " tenemos: " + estanteria[i][j].toString());
                    }

                }
            }

        }

    }
}
