﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio14
{
    public class Bebida_azucarada : Bebida
    {
        private double porcentajeAzucar;
        private bool promocion;

        public Bebida_azucarada(double porcentajeAzucar, bool promocion, int id, int cantidadLitros, double precio, string marca) : base(id, cantidadLitros, precio, marca)
        {
            this.porcentajeAzucar = porcentajeAzucar;
            this.promocion = promocion;
            double descuento = 0.10;
            if (promocion)
            {
                setPrecio(precio - descuento * precio);
            }

        }

        public double getPorcentajeAzucar()
        {
            return porcentajeAzucar;
        }

        public void setPorcentajeAzucar(int porcentajeAzucar)
        {
            this.porcentajeAzucar = porcentajeAzucar;
        }

        public bool isPromocion()
        {
            return promocion;
        }

        public void setPromocion(bool promocion)
        {
            this.promocion = promocion;
        }

        public new double getPrecio()
        {
            if (isPromocion())
            {
                return base.getPrecio() * 0.9;
            }
            else
            {
                return base.getPrecio();
            }
        }

    }
}
