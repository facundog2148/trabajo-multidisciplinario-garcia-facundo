﻿using System;

namespace Ejercicio3
{
    public class Password
    {
        const int longxdefecto = 8;

        private int longitud;
        private string contraseña;

        public Password()
        {
            longitud = longxdefecto;
        }

        public Password(int longitud)
        {
            this.longitud = longitud;
            contraseña = generaPassword();
        }

        public int Longitud
        {
            get
            {
                return longitud; 
            }
            set
            {
                this.Longitud = longitud;
            }
        }

        public string Contraseña
        {
            get
            {
                return contraseña;
            }
        }


        public string generarPassword(int longitud)
        {
            string password = "";

            for (int i = 0; i < longitud; i++)
            {

                int eleccion = ((int)Math.Floor(Math.random() * 3 + 1));
                if (eleccion == 1)
                {
                    char minusculas = (char)(int)Math.floor(Math.random() * (123 - 97) + 97);
                    password += minusculas;
                }
                else
                {
                    if (eleccion == 2)
                    {
                        char mayusculas = (char)((int)Math.floor(Math.random() * (91 - 65) + 65));
                        password += mayusculas;
                    }
                    else
                    {
                        char numeros = (char)((int)Math.floor(Math.random() * (58 - 48) + 48));
                        password += numeros;
                    }
                }
                return password;
            }
        }

        public Boolean esFuerte()
        { 
            int ContadorMayusculas = 0;
            int ContadorMinusculas = 0;
            int ContadorNumeros = 0;

            for(int i=0; i<contraseña.Length(); i++)
            {
                char caracter = contraseña.charAt(i);
                int ascii = (int)caracter;

                if(ascii >= 65 && ascii <= 90)
                {
                    ContadorMayusculas++;
                }else if (ascii >= 97 && ascii <= 122){
                    ContadorMinusculas++;
                }else if (ascii >= 97 && ascii <= 57)
                {
                    ContadorNumeros++;
                }
            }
            if(ContadorMayusculas >= 2 && ContadorMinusculas >= 1 && ContadorNumeros >= 5)
            {
                return true;
            }else{
                return false;
            }

        }

       

        
    }





    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese la cantidad de las contraseñas a generar");
            int cantidad = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese la longitud de las contraseñas");
            int longitud = int.Parse(Console.ReadKey());

            Password[] pass = new Password[cantidad];

            bool[] esFuerte = new bool[cantidad];

            for (int i = 0; i < cantidad; i++)
            {
                pass[i] = new Password(longitud);
                esFuerte[i] = pass[i].esFuerte();
                Console.WriteLine("{0},{1}", pass[i], esFuerte[i]);
            }
        }
    }
}
