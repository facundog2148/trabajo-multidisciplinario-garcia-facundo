﻿using System;

namespace Ejercicio5
{
    public class Serie : Entregable
    {
        private string titulo;
        private int numeroTemporadas = 3;
        private Boolean entregado;
        private string genero;
        private string creador;


        public Serie()
        {
            
        }
        public Serie(string titulo, string creador)
        {
            this.titulo = titulo;
            this.creador = creador;
        }
        public Serie(string titulo, int numeroTemporadas, string genero, string creador)
        {
            this.titulo = titulo;
            this.numeroTemporadas = numeroTemporadas;
            this.entregado = false;
            this.genero = genero;
            this.creador = creador;
        }

        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                this.Titulo = titulo;
            }
        }

        public int getnumeroTemporadas()
        {
            return numeroTemporadas;
        }
        public int NumeroTemporadas
        {
            set
            {
                this.NumeroTemporadas = numeroTemporadas;
            }
        }

        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                this.Genero = genero;
            }
        } 

        public string Creador
        {
            get
            {
                return creador;
            }
            set
            {
                this.Creador = creador;
            }
        }

        public void entregar()
        {
            entregado = true;
        }

        public void devolver()
        {
            entregado = false;
        }

        public Boolean isEntregado()
        {
            bool entrega = false;
            if (entregado == true)
            {
                entrega = true;
            }
            return entrega;
        }

        public int compareTo(Object a)
        {
            int estado = menor;
            Serie s = (Serie)a;
            if (numeroTemporadas > s.getnumeroTemporadas())
            {
                estado = mayor;
            }else if(numeroTemporadas == s.getnumeroTemporadas())
            {
                estado = igual;
            }
            return estado;
     
        }
    }
    
    public class videojuego : Entregable
    {
        private string titulo;
        private int horasEstimadas = 100;
        private Boolean entregado;
        private string genero;
        private string compañia;

        public videojuego()
        {
            
        }

        public videojuego(string titulo, int horasEstimadas)
        {
            this.titulo = titulo;
            this.horasEstimadas = horasEstimadas;
        }

        public videojuego(string titulo, int horasEstimadas, string genero, string compañia)
        {
            this.titulo = titulo;
            this.horasEstimadas = horasEstimadas;
            this.entregado = false;
            this.genero = genero;
            this.compañia = compañia;
        }

        public string Titulo
        {
            get
            {
                return titulo;
            }
            set
            {
                this.Titulo = titulo;
            }
        }

        public int gethorasEstimadas()
        {
            return horasEstimadas;
        }
        public int HorasEstimadas
        {
            set
            {
                this.HorasEstimadas = horasEstimadas;
            }
        }

        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                this.Genero = genero;
            }
        }

        public string Compañia
        {
            get
            {
                return compañia;
            }
            set
            {
                this.Compañia = compañia;
            }
        }

        public void entregar()
        {
            entregado = true;
        }

        public void devolver()
        {
            entregado = false;
        }

        public Boolean isEntregado()
        {
            bool entrega = false;
            if(entregado == true)
            {
                entrega = true;
            }
            return entrega;
        }

        public int compareTo(Object a)
        {
            int estado = menor;
            videojuego s = (videojuego)a;
            if (horasEstimadas > s.gethorasEstimadas())
            {
                estado = mayor;
            }
            else if (numeroTemporadas == s.gethorasEstimadas())
            {
                estado = igual;
            }
            return estado;

        }


    }

    public interface Entregable
    {
        public void entregar();
        public void devolver();
        public Boolean isEntregado();
        public int compareTo(object a);
    }
    class Program
    {
        static void Main(string[] args)
        {
            Serie[] series = new Serie[5];

            Videojuego[] videojuegos = new Videojuego[5];

            series[0] = new Serie();
            series[1] = new Serie("Grey´s Anatomy", "");
            series[2] = new Serie("Breakin Bad", "Tomi");
            series[3] = new Serie("Friends", 2, "Terror", "Roy");
            series[4] = new Serie("Los simpsons", 5, "Comedia", "Aron");

            videojuegos[0] = new Videojuego();
            videojuegos[1] = new Videojuego("Juego 1", 5);
            videojuegos[2] = new Videojuego("Juego 2", 15);
            videojuegos[3] = new Videojuego("Juego 3", 20, "Accion", "Ubisoft");
            videojuegos[4] = new Videojuego("Juego 4", 100, "Moba", "Riot");

            series[1].entregar();
            series[2].entregar();

            videojuegos[1].entregar();
            videojuegos[2].entregar();
        }
    }
}
