﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio10_
{
    class Baraja
    {
        private List<Carta> cartas = new List<Carta>();
        private List<Carta> cartas_dadas = new List<Carta>();
        public Baraja()
        {
            for (int i = 1; i < 11; i++)
                this.cartas.Add(new Carta((i >= 8) ? i + 2 : i, "ESPADA"));
            for (int i = 1; i < 11; i++)
                this.cartas.Add(new Carta((i >= 8) ? i + 2 : i, "BASTO"));
            for (int i = 1; i < 11; i++)
                this.cartas.Add(new Carta((i >= 8) ? i + 2 : i, "ORO"));
            for (int i = 1; i < 11; i++)
                this.cartas.Add(new Carta((i >= 8) ? i + 2 : i, "COPA"));
        }
        public void barajar()
        {
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            List<Carta> cartas_tmp = new List<Carta>();
            for (int i = 0; i < 40; i++)
            {
                int pos = rdn.Next(this.cartas.Count);
                cartas_tmp.Add(this.cartas[pos]);
                this.cartas.RemoveAt(pos);
            }
            this.cartas = cartas_tmp;
        }
        public Carta siguienteCarta()
        {
            if (this.cartas.Count > 0)
            {
                this.cartas_dadas.Add(this.cartas[0]);
                this.cartas.RemoveAt(0);
                return this.cartas_dadas[this.cartas.Count - 1];
            }
            return null;
        }
        public Carta[] darCartas(int cant)
        {
            if (this.cartas.Count >= cant)
            {
                this.cartas_dadas.AddRange(cartas.Take(cant));
                this.cartas.RemoveRange(this.cartas.Count - 1 - cant, cant);
                return cartas_dadas.TakeLast(cant).ToArray();
            }
            return null;
        }
        public void cartasMonton()
        {
            if (this.cartas_dadas.Count == 0)
            {
                Console.WriteLine("No se dio ninguna carta.");
                return;
            }

        }
        public void cartasBaraja()
        {
            if (this.cartas.Count == 0)
            {
                Console.WriteLine("No quedan mas cartas en la baraja.");
                return;
            }

        }
    }
}
