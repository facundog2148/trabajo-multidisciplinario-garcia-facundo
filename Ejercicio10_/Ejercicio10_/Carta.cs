﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio10_
{
    class Carta
    {
        private int Numero;
        private string Palo;

        public Carta(int Numero, string Palo)
        {
            this.Numero = Numero;
            this.Palo = Palo;
        }
        public int getNumero()
        {
            return Numero;
        }
        public void setNumero(int Numero)
        {
            this.Numero = Numero;
        }
        public String getPalo()
        {
            return Palo;
        }
        public void setPalo(String Palo)
        {
            this.Palo = Palo;
        }
    }
}
