﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12bis
{
    public abstract class Empleado
    {
        public int PLUS = 300;
        private string nombre;
        private int edad;
        private double salario;

        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public int getEdad()
        {
            return edad;
        }

        public void setEdad(int edad)
        {
            this.edad = edad;
        }

        public double getSalario()
        {
            return salario;
        }

        public void setSalario(double salario)
        {
            this.salario = salario;
        }

        public Empleado()
        {
        }

        public Empleado(String nombre, int edad, double salario)
        {

            this.nombre = nombre;
            this.edad = edad;
            this.salario = salario;
        }



    }
}
