﻿using System;

namespace Ejercicio12bis
{
    class Program
    {
        static void Main(string[] args)
        {
            Comercial emplcom1 = new Comercial("Juan", 38, 1000, 500);
            Comercial emplcom2 = new Comercial("Carlitos", 29, 1000, 300);
            Repartidor emplrep1 = new Repartidor("Martin", 22, 450, "zona 3");


            emplcom1.plus();
            emplcom2.plus();
            emplrep1.plus();

            Console.WriteLine(emplcom1);
            Console.WriteLine(emplcom2);
            Console.WriteLine(emplrep1);

        }
    }
}
