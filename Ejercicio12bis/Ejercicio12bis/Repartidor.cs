﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio12bis
{
    class Repartidor : Empleado, Plus
    {
        private String Zona;

        public String getZona()
        {
            return Zona;
        }

        public void setZona(String Zona)
        {
            this.Zona = Zona;
        }

        public Repartidor()
        {

        }


        public Repartidor(String nombre, int edad, double salario, String Zona) : base(nombre, edad, salario)
        {
            this.Zona = Zona;
        }

        public void plus()
        {
            if (getEdad() < 25 && getZona().Equals("zona 3"))
            {
                Console.WriteLine("El repartidor recibirá el Plus de " + PLUS);
                setSalario(getSalario() + PLUS);
            }
        }

    }
}

