﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Ejercicio12bis
{
    public class Comercial : Empleado , Plus
    {
        private double comision;

        public double getComision()
        {
            return comision;
        }

        public void setComision(double comision)
        {
            this.comision = comision;
        }

        public Comercial()
        {
        }

        public Comercial(String nombre, int edad, int salario, double comision) : base(nombre, edad, salario)
        {
            this.comision = comision;
        }
        public void plus()
        {
            if (getEdad() > 30 && getComision() > 200)
            {
                Console.WriteLine("El comercial recibira el Plus de " + PLUS);
                setSalario(getSalario() + PLUS);


            }

        }
    }

}
   
