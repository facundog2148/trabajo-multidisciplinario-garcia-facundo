﻿using System;

namespace Ejercicio2
{
    public class Persona
    {
        private string nombre;
        private int edad;
        private string DNI;
        private char sexo;
        private double peso;
        private double altura;

        const char sexo_x_defecto = "H";
        const double pesomenor = -1;
        const double pesoideal = 0;
        const double sobrepeso = 1;


        public Persona()
        {

        }

        public Persona(string nombre, int edad, char sexo)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.sexo = sexo;
        }

        public Persona(string nombre, int edad, string DNI, char sexo, double peso, double altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.DNI = DNI;
            this.sexo = sexo;
            comprobarSexo();
            this.peso = peso;
            this.altura = altura;
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                this.Nombre = nombre;
            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                this.Edad = edad;
            }
        }

        public char Sexo
        {
            get
            {
                return sexo;
            }
            set
            {
                this.Sexo = sexo;
            }
        }

        public double Peso
        {
            get
            {
                return peso;
            }
            set
            {
                this.Peso = peso;
            }
        }

        public double Altura
        {
            get
            {
                return altura;
            }
            set
            {
                this.Altura = altura;
            }
        }

        public Boolean EsMayorDeEdad()
        {
            Boolean MayoriaDeEdad = false;
            if(edad >= 18)
            {
                MayoriaDeEdad = true;
            }
            return MayoriaDeEdad; 
        }

        private void comprobarSexo()
        {
            if(sexo != "M" && sexo != "H")
            {
                sexo = sexo_x_defecto;
            }
            Console.WriteLine(sexo);
        }

        public void calcularIMC(int peso, double altura)
        {
            double calculoPeso;
            calculoPeso = peso / altura * altura;
            if (calculoPeso >= 20 && calculoPeso <= 25)
            {
                calculoPeso = pesoideal;
                Console.WriteLine("Tiene un peso ideal para su estatura");
            }else if (calculoPeso < 20)
            {
                calculoPeso = pesomenor;
                Console.WriteLine("Tiene poco peso en relación a su estatura");
            }else
            {
                calculoPeso = sobrepeso;
                Console.WriteLine("Tiene mucho peso en relación a su estatura");
            }

        }

        public double pesoideal(double peso)
        {
            double pesoactual = peso / Math.Pow(altura, 2);
            int pesoideal = 0;
            int sobrepeso = 1;
            int pesomenor = -1;
            if (pesoactual >= 20 && pesoactual <= 25)
            {
                return pesoideal;
            }
            else if (pesoactual < 20)
            {
                return pesomenor;
            }
            else
            {
                return sobrepeso;
            }
        }
    }













    class Program
    {
        static string nombre, edad, sexo, dni, peso, altura;
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese un nombre");
            nombre = Console.ReadLine();

            Console.WriteLine("Ingrese una edad");
            edad = Console.ReadLine();

            Console.WriteLine("Ingrese un sexo");
            sexo = Console.ReadLine();

            Console.WriteLine("Ingrese un dni");
            dni = Console.ReadLine();

            Console.WriteLine("Ingrese un peso");
            peso = Console.ReadLine();

            Console.WriteLine("Ingrese una altura");
            altura = Console.ReadKey();

            string pesoideal = Console.ReadLine();
            string comprobarSexo = Console.ReadLine();

            Persona p1 = new Persona(nombre, Convert.ToInt16(edad), Convert.ToInt16(dni), Convert.ToChar(sexo), Convert.ToInt16(peso), Convert.ToDouble(altura));
            Persona p2 = new Persona(nombre, Convert.ToInt16(edad), Convert.ToChar(sexo));
            Persona p3 = new Persona();


            double Pesoideal = p1.pesoideal(Convert.ToDouble(pesoideal));
            Console.WriteLine(Pesoideal);
            Console.ReadLine();

            p2.EsMayorDeEdad();
            Console.ReadLine();
        }
    }
}
