﻿using System;

namespace Ejercicio7
{
    public class Raices
    {
        private double a;
        private double b;
        private double c;

        public Raices(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public double Discriminante
        {
            get
            {
                return Math.Pow(b,2) - (4 * a * c);
            }
        }

        public void obtenerRaices()
        {
            double x1 = (-b + Math.Sqrt(Discriminante)) / (2 * a);
            double x2 = (-b - Math.Sqrt(Discriminante)) / (2 * a);

            Console.WriteLine("Solucion X1");
            Console.WriteLine(x1);
            Console.WriteLine("Solucion X2");
            Console.WriteLine(x2);
        }

        public bool obtenerRaiz()
        {
            double x = (-b) / (2 * a);
            Console.WriteLine("Unica solución");
            Console.Write(x);
        }

        public bool tieneRaices()
        {
            return Discriminante > 0;
        }

        public bool tieneRaiz()
        {
            return Discriminante == 0;
        }

        public void calcular()
        {
            if (tieneRaices())
            {
                obtenerRaices();
            }else if (tieneRaiz())
            {
                obtenerRaiz();
            }
            else
            {
                Console.WriteLine("No tiene soluciones");
            }
            Console.ReadKey();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Raices Ecuacion = new Raices(4, 4, 4);
            Ecuacion.calcular();
        }
    }
}
