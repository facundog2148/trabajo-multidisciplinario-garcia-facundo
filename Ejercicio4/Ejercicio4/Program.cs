﻿using System;

namespace Ejercicio4
{
    public class Electrodomestico
    {
        const string color_x_defecto = "blanco";
        const char cons_energ_x_defecto = 'F';
        const double precio_x_defecto = 5;
        protected double preciobase = 100;
        protected string color = "Blanco";
        protected char consumoEnergetico;
        protected double peso;

        public Electrodomestico()
        {
            
        }

        public Electrodomestico(double preciobase, double peso)
        {
            this.preciobase = preciobase;
            this.peso = peso;
           
        }

        public Electrodomestico(double preciobase, string color, char consumoEnergetico, double peso)
        {
            this.preciobase = preciobase;
            this.color = color;
            comprobarConsumoEnergetico(consumoEnergetico);
            comprobarColor(color);
        }

        public double Preciobase
        {
            get
            {
                return preciobase;
            }
        }

        public string Color
        {
            get
            {
                return color;
            }
        }

        public char ConsumoEnergetico
        {
            get
            {
                return consumoEnergetico;
            }
        }

        public double Peso
        {
            get
            {
                return peso;
            }
        }

        private void comprobarColor(string color)
        {
            string[] colores ={ "blanco", "negro", "rojo", "azul", "gris" };
            Boolean colorEncontrado = false;
            for (int i = 0; i < colores.lenght && !colorEncontrado; i++)
            {
                if (colores[i].Equals(color))
                {
                    colorEncontrado = true;
                }
            }
            if (colorEncontrado)
            {
                this.color = color;
            }
            else
            {
                this.color = color_x_defecto;
            }
        }

        private void comprobarConsumoEnergetico(char consumoEnergetico)
        {
            char[] letras = { 'A', 'B', 'C', 'D', 'E', 'F' };
            Boolean letraEncontrada = false;
            for(int i = 0; i < letras.lenght && !letraEncontrada; i++)
            {
                if(letras[i] == consumoEnergetico)
                {
                    letraEncontrada = true;
                }
            }
            if (letraEncontrada)
            {
                this.consumoEnergetico = consumoEnergetico;
            }
            else
            {
                this.consumoEnergetico = cons_energ_x_defecto;
            }
        }

        public double precioFinal()
        {
            int plus = 0;
            switch (this.consumoEnergetico)
            {
                case 'A':
                    plus = 100;
                    break;
                case 'B':
                    plus = 80;
                    break;
                case 'C':
                    plus = 60;
                    break;
                case 'D':
                    plus = 50;
                    break;
                case 'E':
                    plus = 30;
                    break;
                case "F":
                    plus = 10;
                    break;
            }
            if(this.peso >= 0 && this.peso < 20)
            {
                plus += 10;
            }else if(this.peso >= 20 && this.peso < 50)
            {
                plus += 50;
            }else if(this.peso >= 50 && this.peso < 80)
            {
                plus += 80;
            }else
            {
                plus += 100;
            }
            return plus + this.preciobase;
        }

    }

    public class Lavadora : Electrodomestico
    {
        const int carga_x_defecto = 5;
        private int carga;

        public Lavadora()
        {
            
        }

        public Lavadora(double preciobase, double peso) : base(peso, preciobase)
        {
            this.preciobase = preciobase;
            this.peso = peso;
            
        }

        public Lavadora(double preciobase, string color, double peso, char consumoEnergetico, int carga) : base(peso, color, peso, consumoEnergetico)
        {
            
            this.carga = carga;
        }

        public int Carga
        {
            get
            {
                return carga;
            }
        }

        public class Television : Electrodomestico
        {
            const int resol_x_defecto = 20;
            private int resolucion;
            private Boolean sintonizadorTDT;

            public Television()
            {
               

            }

            public Television(double preciobase, double peso) : base(preciobase, peso)
            {
                this.preciobase = preciobase;
                this.peso = peso;
           
            }

            public Television(double preciobase, string color, double peso, char consumoEnergetico, int resolucion, bool sintonizadorTDT) : base(preciobase, color, peso, consumoEnergetico)
            {
                this.resolucion = resolucion;
                this.sintonizadorTDT = sintonizadorTDT;
            }

              

            public int Resolucion
            {
                get
                {
                    return resolucion;
                }
            }

            public Boolean SintonizadorTDT
            {
                get
                {
                    return sintonizadorTDT;
                }
            }

            public double precioFinal()
            {
                double monto = super.precioFinal();
                if(tamañoPantalla >= 40)
                {
                    monto += preciobase * 0.3;
                }
                if (sintonizadorTDT)
                {
                    monto += 50;
                }
                return monto;
            }

        }

        class Program
        {
            static void Main(string[] args)
            {
                Electrodomestico[] electrodomesticos = new Electrodomestico[10];
                electrodomesticos[0] = new Television();
                electrodomesticos[1] = new Lavadora(2000, 40);
                electrodomesticos[2] = new Lavadora(1200, "Negro", 55, "C", true);
                electrodomesticos[3] = new Lavadora();
                electrodomesticos[4] = new Television(1000, "Negro", 35, "B", 50, true);
                electrodomesticos[5] = new Television(500, 40);
                electrodomesticos[6] = new Television(800, "Azul", 30, "C", 42, false);
                electrodomesticos[7] = new Television(900, "Negro", 45, "A", 55, true);
                electrodomesticos[8] = new Lavadora(1100, "Blanco", 50, "B", false);
                electrodomesticos[9] = new Lavadora(700, "Verde", 45, "E", true);

                int televisores = 0, lavarropas = 0;
                foreach (Electrodomestico elect in electrodomesticos)
                {
                    elect.precioFinal();
                    if (elect is Television)
                    {
                        televisores++;
                    }
                    if (elect is Lavadora)
                    {
                        lavarropas++;
                    }
                }
                Console.WriteLine("Televisores: {0} \n Lavarropas: {1} \n Electrodomesticos: {2}", televisores, lavarropas, televisores + lavarropas);
            }
        }
    }
}
