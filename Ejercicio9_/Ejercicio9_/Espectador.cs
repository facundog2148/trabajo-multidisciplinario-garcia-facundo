﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9_
{
    class Espectador
    {
        private string Nombre;
        private int Edad;
        private float Dinero;
        public Espectador(string Nombre, int Edad, float Dinero)
        {
            this.Nombre = Nombre;
            this.Edad = Edad;
            this.Dinero = Dinero;
        }
        public string getNombre
        {
            get
            {
                return this.Nombre;
            }
        }
        public int getEdad
        {
            get
            {
                return this.Edad;
            }
        }
        public float getDinero
        {
            get
            {
                return this.Dinero;
            }
        }
        public string setNombre
        {
            set
            {
                this.Nombre = value;
            }
        }
        public int setEdad
        {
            set
            {
                this.Edad = value;
            }
        }
        public float setDinero
        {
            set
            {
                this.Dinero = value;
            }
        }
        public bool tieneEdad(int edad)
        {
            return (this.Edad >= edad);
        }
        public bool tieneDinero(float dinero)
        {
            return (this.Dinero >= dinero);
        }
    }
}
