﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9_
{
    class Asiento
    {
        private char Letra;
        private int Fila;
        private Espectador espectador = null;

        public Asiento(char Letra, int Fila)
        {
            this.Letra = Letra;
            this.Fila = Fila;
        }
        public char getLetra
        {
            get
            {
                return this.Letra;
            }
        }
        public int getFila
        {
            get
            {
                return this.Fila;
            }
        }
        public Espectador getEspectador
        {
            get
            {
                return this.espectador;
            }
        }
        public char setLetra
        {
            set
            {
                this.Letra = value;
            }
        }
        public int setFila
        {
            set
            {
                this.Fila = value;
            }
        }
        public Espectador setEspectador
        {
            set
            {
                this.espectador = value;
            }
        }
        public bool Ocupado()
        {
            return (this.espectador != null);
        }
    }
}
