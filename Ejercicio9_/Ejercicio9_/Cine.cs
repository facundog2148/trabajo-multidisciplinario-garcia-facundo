﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9_
{
    class Cine
    {
        private List<List<Asiento>> asientos = new List<List<Asiento>>();
        private Pelicula pelicula;
        private float PrecioEntrada;

        public Cine(int filas, int columnas, float precio, Pelicula pelicula)
        {
            this.PrecioEntrada = precio;
            this.pelicula = pelicula;
            int columnas_t = (columnas > 26) ? 26 : columnas;
            for (int i = 0; i < filas; i++)
            {
                List<Asiento> fila_temp = new List<Asiento>();
                for (int z = 0; z < columnas_t; z++)
                {
                    fila_temp.Add(new Asiento((char)(z + 65), i + 1));
                }
                this.asientos.Add(fila_temp);
            }
        }
        public List<List<Asiento>> getAsientos
        {
            get
            {
                return this.asientos;
            }
        }
        public float getPrecio
        {
            get
            {
                return this.PrecioEntrada;
            }
        }
        public Pelicula getPelicula
        {
            get
            {
                return this.pelicula;
            }
        }
        public List<List<Asiento>> setAsientos
        {
            set
            {
                this.asientos = value;
            }
        }
        public float setPrecio
        {
            set
            {
                this.PrecioEntrada = value;
            }
        }
        public Pelicula setPelicula
        {
            set
            {
                this.pelicula = value;
            }
        }

        public bool hayLugar()
        {
            foreach (List<Asiento> fila in asientos)
            {
                foreach (Asiento a in fila)
                {
                    if (a.getEspectador == null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private int letra(char c)
        {
            int colum = -1;
            if (((int)c >= 65 && (int)c <= 90) || (int)c >= 97 && (int)c <= 122)
            {
                if ((int)c <= 90)
                {
                    colum = (int)c - 65;
                }
                else
                {
                    colum = (int)c - 97;
                }
            }
            return colum;
        }
        public bool hayLugarButaca(int fila, char c)
        {
            int colum = letra(c);
            if (colum == -1)
            {
                return false;
            }
            if (fila >= asientos.Count)
            {
                return false;
            }
            return (asientos[fila][colum].getEspectador == null);
        }
        public bool sePuedeSentar(Espectador e)
        {
            return (e.tieneEdad(this.pelicula.getEdadMinima) && e.tieneDinero(this.PrecioEntrada));
        }
        public void sentar(int fila, char c, Espectador e)
        {
            this.asientos[fila][letra(c)].setEspectador = e;
        }

        public Asiento getAsiento(int fila, char col)
        {
            return this.asientos[fila][this.letra(col)];
        }

        public int getFilas()
        {
            return asientos.Count;
        }
    }
}
