﻿using System;

namespace Ejercicio9_
{
    class Program
    {
        static void Main(string[] args)
        {
            Pelicula movie = new Pelicula("Volver al Futuro", 200, 13, "pepito");
            Cine cine = new Cine(7, 3, 200, movie);
            Console.WriteLine(movie);
            Console.WriteLine(cine);

            Espectador espectador1 = new Espectador("pepe", 33, 15);
            Espectador espectador2 = new Espectador("pepito", 19, 18);
            Espectador espectador3 = new Espectador("ana", 32, 25);
            Espectador espectador4 = new Espectador("anita", 21, 40);

            List<Espectador> espectadores = new List<Espectador>();
            espectadores.Add(espectador1);
            espectadores.Add(espectador2);
            espectadores.Add(espectador3);
            espectadores.Add(espectador4);

            Cine.sePuedeSentar(espectadores, cine);

            Console.WriteLine(Cine.hayLugarButaca(cine));
        }
    }
}
