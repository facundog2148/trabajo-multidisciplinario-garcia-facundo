﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio9_
{
    class Pelicula
    {
        private string Titulo;
        private int Duracion;
        private int EdadMinima;
        private string Director;

        public Pelicula(string Titulo, int Duracion, int EdadMinima, string Director)
        {
            this.Titulo = Titulo;
            this.Director = Director;
            this.EdadMinima = EdadMinima;
            this.Duracion = Duracion;
        }
        public string setTitulo
        {
            set
            {
                this.Titulo = value;
            }
        }
        public int setDuracion
        {
            set
            {
                this.Duracion = value;
            }
        }
        public int setEdadMinima
        {
            set
            {
                this.EdadMinima = value;
            }
        }
        public string setDirector
        {
            set
            {
                this.Director = value;
            }
        }
        public string getTitulo
        {
            get
            {
                return this.Titulo;
            }
        }
        public int getDuracion
        {
            get
            {
                return this.Duracion;
            }
        }
        public string getDirector
        {
            get
            {
                return this.Director;
            }
        }
        public int getEdadMinima
        {
            get
            {
                return this.EdadMinima;
            }
        }
    }
}
