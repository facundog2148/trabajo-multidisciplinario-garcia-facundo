﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio13
{
    public class NoPerecedero : Producto
    {
        private string tipo;

        public NoPerecedero(string nombre, int precio, string tipo) : base(nombre, precio)
        {
            this.tipo = tipo;
        }

        public String getTipo()
        {
            return tipo;
        }

        public void setTipo(String tipo)
        {
            this.tipo = tipo;
        }


    }
}
