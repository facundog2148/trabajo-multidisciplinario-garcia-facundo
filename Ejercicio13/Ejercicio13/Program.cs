﻿using System;

namespace Ejercicio13
{
    class Program
    {
        static void Main(string[] args)
        {
            Producto[] productos = new Producto[3];

            productos[0] = new Producto("Aceite", 2);
            productos[1] = new Perecedero("Pan", 5, 1);
            productos[2] = new Perecedero("Pollo", 20, 2);
            productos[3] = new Perecedero("Pescado", 30, 5);
            productos[4] = new NoPerecedero("Leche", 1, "Lacteos");


            double total = 0.0;
            for (int i = 0; i < productos.Length; i++)
            {
                total += productos[i].calcular(5);
            }

            Console.WriteLine("El total es: " + total);
            Console.ReadLine();

        }
    }
}
