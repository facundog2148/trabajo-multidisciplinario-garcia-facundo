﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio13
{
    public class Producto
    {
        public string nombre;
        public double precio;

        public Producto(string nombre, double precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
        public String getNombre()
        {
            return nombre;
        }

        public void setNombre(String nombre)
        {
            this.nombre = nombre;
        }

        public double getPrecio()
        {
            return precio;
        }

        public void setPrecio(double precio)
        {
            this.precio = precio;
        }
        public double calcular(int cantidad)
        {
            return precio * cantidad;
        }

    }
}
