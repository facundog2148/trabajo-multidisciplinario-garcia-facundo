﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio13
{
    public class Perecedero : Producto
    {
        private int diasacaducar;

        public Perecedero(string nombre, double precio, int diascaducar) : base(nombre, precio)
        {
            this.diasacaducar = diascaducar;
        }

        public int getDiasacaducar()
        {
            return diasacaducar;
        }

        public void setDiasacaducar(int diasacaducar)
        {
            this.diasacaducar = diasacaducar;
        }

        public new int calcular(int cantidad)
        {

            int precioFinal = calcular(cantidad);

            switch (diasacaducar)
            {
                case 1:
                    precioFinal /= 4;
                    break;
                case 2:
                    precioFinal /= 3;
                    break;
                case 3:
                    precioFinal /= 2;
                    break;
            }

            return precioFinal;


        }
    }
}
